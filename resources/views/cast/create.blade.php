@extends('layout.master')
@section('judul')
    Halaman Tambah Pemeran
@endsection

@section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="form-group">
            <label>Nama Pemeran</label>
            <input type="text" class="form-control" name="nama" placeholder="Masukkan nama pemeran">
        </div> 
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" name="umur" placeholder="Masukkan umur pemeran. Cukup angkanya saja. Contoh : 25">
        </div> 
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea class="form-control" name="bio" placeholder="Masukkan bio"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection