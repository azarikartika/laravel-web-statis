<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/data-tables', function(){
    return view('page.data-tables');
});
Route::get('/table', function(){
    return view('page.table');
});

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@registerForm');

Route::post('/welcome', 'AuthController@welcome');

//CRUD CAST
//Mengarah ke Form Create Data
Route::get('/cast/create', 'CastController@create');
//Menyimpan data ke table Cast
Route::post('/cast', 'CastController@store');
//Memanggil semua data dari table cast
Route::get('/cast', 'CastController@index');
//Memanggil data berdasarkan id di table cast
Route::get('/cast/{cast_id}', 'CastController@show');

//mengarah ke Form Edit Data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//Update data ke table cast
Route::put('cast/{cast_id}', 'CastController@update');

//DELETE data
Route::delete('/cast/{cast_id}', 'CastController@destroy');