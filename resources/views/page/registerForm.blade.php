@extends('layout/master')
@section('judul')
    Form Sign Up
@endsection
@section('content')
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="post">
        @csrf
        <!-- Membuat token supaya tidak terjadi tabrakan data, setiap ada form wajib pake token -->
        <label>First Name :</label><br><br>
        <input type="text" name="first_name" required><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="last_name" required><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" id="male" name="gender" value="male" required>Male<br>
        <input type="radio" id="female" name="gender" value="female" required>Female<br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Japan">Japan</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" value="" rows="10" cols="30" required></textarea><br><br>
        <input type="submit" name="submit" value="Sign Up">
    </form>
@endsection