@extends('layout.master')
@section('judul')
    Halaman Detail Pemeran
@endsection

@section('content')
    <p> Nama : {{ $cast->nama }}</p>
    <p> Umur : {{ $cast->umur }}</p>
    <p> Bio  : {{ $cast->bio }}</p>

    <a href="/cast" class="btn btn-primary my-3"><i class="fas fa-backward"></i> Kembali</a>
@endsection
