@extends('layout.master')
@section('judul')
    Halaman Daftar Pemeran
@endsection
@push('scripts')
    <script src="{{ asset('layout/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('layout/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#cast").DataTable();
        });
    </script>
@endpush

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css" />
@endpush

@section('content')
    <a href="/cast/create" class="btn btn-primary my-3"><i class="fas fa-plus"></i> Tambah Pemeran</a>

    <table id="cast" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }} Tahun</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm"><i class="fas fa-info"></i> Detail</a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i> Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <h3>Data kosong!</h3>
            @endforelse
        </tbody>
    </table>
@endsection
