<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerForm()
    {
        // return view('page/registerForm');
        return view('page.registerForm');
    }
    
    public function welcome(Request $request)
    {
        // dd($request->all()); //dd untuk dumping/debug
        $first_name = $request['first_name']; 
        $last_name = $request['last_name']; 
        
        return view('page/welcome', compact('first_name', 'last_name'));
    }
}
