@extends('layout/master')
@section('judul')
    Halaman Selamat Datang
@endsection
@section('content')
    <h1>Selamat Datang {{ $first_name }} {{ $last_name }}!</h1>
    <h4>Terima kasih telah bergabung di Website kami. Media Belajar kita bersama!</h4>
@endsection
